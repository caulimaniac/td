package Term

import "fmt"

const (
    COLOR_GREEN  = "\033[0;32m"
    COLOR_YELLOW = "\033[1;33m"
    COLOR_RED    = "\033[0;31m"
    CLEAR_FORMAT = "\033[0;0m"

    LINE_CLEAR      = "\x1b[2K"
    STYLE_UNDERLINE = "\033[4m"
    STYLE_BOLD      = "\033[1m"
)

func LINES_UP(lines int) string {
    return fmt.Sprintf("\033[%dA", lines)
}
