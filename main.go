package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/caulimaniac/td/Urgency"
)

type Todo struct {
    Name string `json:"name"`
    Tags []string `json:"tags"`
    Action string `json:"action"`
    Deadline string `json:"deadline"`
    Urgency Urgency.Urgency `json:"urgency"`
}

var ( 
    USER_HOME_PATH string
    todos []*Todo
)

func main(){
    expandSquashedArguments()
    parseArguments(ARGUMENTS_GLOBAL, true)
    setup()
    fetchTodos()

    var action string
    if arguments.Help {
        action = "help"
    } else if len(arguments.Trailing) > 1 {
        action = arguments.Trailing[1]
    } else {
        action = "list" }

    switch action {
        case "add", "a":
            parseArguments(ARGUMENTS_ADD, false)
            actionAdd(strings.Join(arguments.Trailing[2:], " "))
            writeChangesWithHistory()

        case "done", "d":
            parseArguments(ARGUMENTS_DONE, false)
            actionDone()
            writeChangesWithHistory()

        case "undo", "u":
            parseArguments(ARGUMENTS_UNDO, false)
            actionUndo()
            writeChanges()

        case "list", "l":
            parseArguments(ARGUMENTS_LIST, false)
            actionList()

        case "serve", "s":
            parseArguments(ARGUMENTS_SERVE, false)
            actionServe()

        case "history", "h":
            parseArguments(ARGUMENTS_HISTORY, false)
            displayHistory()

        default:
            fmt.Printf("Unknown action '%s'\n\n", arguments.Trailing[0])
            displayHelp()
            os.Exit(1) }
}
