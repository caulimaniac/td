# Contributing

Generally you are free to contribute to td.
When fixing spelling or bugs feel free to just submit a MR.

If you intend to implement major changes or features 
please first create an issue explaining how your change would improve td.
Note that td is intended to be 'minimal'. Please ensure your feature is aligned
with the philosophy of td.

## Philosophy lol

_Please note that this is what I intend td to be. You may used td for whatever
you like - obviously - but if you intend to contribute it would be great if you
took this into consideration._

td is a todo manager. It's not a callendar, scheduler, project planner or
progress tracker. Todos added to todo should be concise and clearly defined.
For example, adding a todo called 'Learn Spanish' would be bad because it is
unclear when this todo is actually complete. Instead, consider a todo like
'Read the first chapter of spanish book XYZ' or 'Clear all new anki cards'.

Another goal of td is to integrate with as many programs as possible. 
Preferably in a way which on completion of the todo reopens the program 
in the same state as when the todo was added.

A big concern of td is ease-of-use. Often times using todo managers is a
hassle. You have to open the program, click add, fill out random fields or
type a long, complicated command. td tries to improve this by having easy
and short syntax and (more-or-less) intelligent guessing of intentions.
The goal here is to not make you feel like you're wasting time managing todos.

## Non-interests

The following is a list of features I do not intend to add into td 
because because I don't think they provide enough value for the implementation 
or maintenance effort or I just plain don't care about them.
If you think I am wrong feel free to fork the project.

- Official Windows / MacOS support
- Any argument parsing library
- Recurring todos

