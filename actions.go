package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

var (
    MULTI_ACTION_CONFIG = map[string]string{
        "mpv": " --{ %--start=[0-9]+% %--end=[0-9]+% %( |^)[^-][^ ]*% --}",
        "imv": " %( |^)[^-][^ ]*%",
        "test": " %( |^)[^-][^ ]*%" }
)

func actionUndo(){
    historyJson, err := os.ReadFile(arguments.Config + "/history.json")
    checkError(err)
    var history []Action
    json.Unmarshal(historyJson, &history)

    if len(history) < 1 { checkError(errors.New("Already at earliest point in history")) }

    lastTime := history[len(history)-1].Time
    for i := len(history) - 1; i >= 0; i-- {
        if history[i].Time != lastTime { break }

        if history[i].Action == ActionAdd {
            _, todos = splitTodos(func(t *Todo) bool { 
                return t.Name == history[i].Todo.Name })
        } else {
            todos = append(todos, history[i].Todo) }

        history = history[:i]}

    writeHistory(history)
}

func actionServe(){
    http.HandleFunc("/add", actionServeAdd)
    http.ListenAndServe(arguments.ServeHost + ":" + arguments.ServePort, nil)
}

func triggerActions(todos []*Todo) {
    programCounts := map[string]int{}
    for _, todo := range todos {
        programName := strings.SplitN(todo.Action, " ", 2)[0]
        programCounts[programName]++ }

    multiCommands := map[string]string{}
    for _, todo := range todos {
        if todo.Action != "" {
            programName := strings.SplitN(todo.Action, " ", 2)[0]
            if programCounts[programName] < 2 || len(MULTI_ACTION_CONFIG[programName]) == 0 {
                err := exec.Command("/usr/bin/bash", "-c", todo.Action).Start()
                checkError(err) 
                continue }

            if len(multiCommands[programName]) == 0 { 
                multiCommands[programName] = programName }

            multiCommands[programName] += assembleMultiParameter(
                MULTI_ACTION_CONFIG[programName],
                strings.SplitN(todo.Action, " ", 2)[1]) }}

    for _, multiCommand := range multiCommands {
        err := exec.Command("/usr/bin/bash", "-c", multiCommand).Start()
        checkError(err) }
}

func assembleMultiParameter(argumentBlueprint string, arguments string) string {
    parsedConfig := argumentBlueprint

    regexSection := regexp.MustCompile(`%([^%]*)%`)
    matches := regexSection.FindAllStringSubmatch(parsedConfig, -1)

    for _, regexScript := range matches {
        customRegex := regexp.MustCompile(regexScript[1])
        parsedConfig = strings.ReplaceAll(
            parsedConfig,
            regexScript[0],
            customRegex.FindString(arguments)) }

    return parsedConfig
}
