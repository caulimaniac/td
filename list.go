package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"strings"

	"github.com/gerow/pager"
	"gitlab.com/caulimaniac/td/Color"
	"gitlab.com/caulimaniac/td/Urgency"
	"golang.org/x/exp/slices"
	"golang.org/x/term"
)

func getPrintCompact() string {
    printLines := ""
    for _, todo := range todos {
        var colorPrefix, colorPostfix string
        if !arguments.ListNoColor {
            colorPrefix = string(Color.UrgencyToColor(todo.Urgency))
            colorPostfix = string(Color.CLEAR) }
        printLines += fmt.Sprintf(
            "%s%s%s\n",
            colorPrefix,
            todo.Name,
            colorPostfix) }

    return printLines
}

func printJson() {
    todosJson, err := json.Marshal(todos)
    checkError(err)
    fmt.Print(string(todosJson))
}

func getTermWidthPercent(percent int) int {
    termWidth, _, err := term.GetSize(0)
    checkError(err)
    return (termWidth - 2 * 4) * percent / 100 // - 2 * 4 to include the spacers
                                               // between columns
}

func getPrintTable() string {
    printLines := ""

    urgencyLength := generateTodoPropertyLength(func(todo *Todo) int { 
        return len(Urgency.ToString(todo.Urgency)) }, len("Urgency"), getTermWidthPercent(10))
    nameLength := generateTodoPropertyLength(func(todo *Todo) int { 
        return len(todo.Name) }, len("Todo"), getTermWidthPercent(40))
    tagsLength := generateTodoPropertyLength(func(todo *Todo) int { 
        return len(strings.Join(todo.Tags, ", ")) }, len("Tags"), getTermWidthPercent(20))
    deadlineLength := generateTodoPropertyLength(func(todo *Todo) int { 
        return len(todo.Deadline) }, len("Deadline"), getTermWidthPercent(20))
    actionLength := generateTodoPropertyLength(func(todo *Todo) int { 
        return len(strings.Split(todo.Action, " ")[0]) }, len("Action"), getTermWidthPercent(10))

    var colorPrefixHeader, colorPostfixHeader string
    if !arguments.ListNoColor {
        colorPrefixHeader = string(Color.BOLD)
        colorPostfixHeader = string(Color.CLEAR) }
    printLines += fmt.Sprintf(
        "%s%s  %s  %s  %s  %s%s\n", 
        colorPrefixHeader,
        padOrEllipse("Urgency", urgencyLength), 
        padOrEllipse("ToDo", nameLength), 
        padOrEllipse("Tags", tagsLength),
        padOrEllipse("Deadline", deadlineLength),
        padOrEllipse("Action", actionLength),
        colorPostfixHeader)

    for _, todo := range todos {
        paddedName     := padOrEllipse(todo.Name, nameLength)
        paddedTags     := padOrEllipse(strings.Join(todo.Tags, ", "), tagsLength)
        paddedAction   := padOrEllipse(strings.Split(todo.Action, " ")[0], actionLength)
        paddedDeadline := padOrEllipse(todo.Deadline, deadlineLength)
        paddedUrgency  := padOrEllipse(Urgency.ToString(todo.Urgency), urgencyLength)

        var colorPrefix, colorPostfix string
        if !arguments.ListNoColor {
            colorPrefix = string(Color.UrgencyToColor(todo.Urgency))
            colorPostfix = string(Color.CLEAR) }
        printLines += fmt.Sprintf(
            "%s%s  %s  %s  %s  %s%s\n", 
            colorPrefix, 
            paddedUrgency,
            paddedName, 
            paddedTags,
            paddedDeadline,
            paddedAction,
            colorPostfix) }

    return printLines
}

func getTag(str string) (string, error) {
    possibleTags := []string{}
    for _, todo := range todos {
        for _, tag := range todo.Tags {
            if tag == str {
                return str, nil
            } else if slices.Contains(possibleTags, tag) {
                continue
            } else if strings.HasPrefix(strings.ToLower(tag), strings.ToLower(str)) {
                possibleTags = append(possibleTags, tag) }}}

    if len(possibleTags) == 0 {
        return "", errors.New("No tag named " + str) }
    if len(possibleTags) > 1 {
        return "", errors.New("Multiple tags with prefix: " + str) }

    return possibleTags[0], nil
}

func removeUnwantedTodos(){
    // Tag filter
    if len(arguments.Trailing) > 2 {
        tag, err := getTag(arguments.Trailing[2])
        checkError(err)
        todos = slices.DeleteFunc(todos, func(todo *Todo) bool {
            for _, todoTag := range todo.Tags {
                if tag == todoTag { return false }}
            return true })}

    // Urgency filter
    if arguments.ListUrgency != Urgency.UNRATED {
        todos = slices.DeleteFunc(todos, func(todo *Todo) bool {
            if arguments.ListUrgency != todo.Urgency { return true }
            return false })}

    if !arguments.ListAll {
        todos = slices.DeleteFunc(todos, func(todo *Todo) bool {
            if todo.Name[0] == '.' { return true }
            return false })}
}

func actionList() {
    parseAutoUrgencies()
    removeUnwantedTodos()
    sort.Slice(todos, sortTodosIntuitively)
    termWidth, termHeight, err := term.GetSize(0)
    if err != nil { termWidth = 0; termHeight = 0 }
    if termWidth < 100 { arguments.ListCompact = true }

    var printLines string
    if arguments.ListJson {
        printJson()
        return
    } else if arguments.ListCompact {
        printLines = getPrintCompact()
    } else {
        printLines = getPrintTable() }

    longerThanHeight := len(strings.Split(printLines, "\n")) > termHeight
    if longerThanHeight && !arguments.ListNoPager { pager.Open() }
    fmt.Print(printLines)
    if longerThanHeight && !arguments.ListNoPager { pager.Close() }
}

func generateTodoPropertyLength(todoPropertyLength func(*Todo)int, minLength int, maxLength int) int {
    currentLength := minLength
    for _, todo := range todos {
        if todoPropertyLength(todo) > currentLength {
            currentLength = todoPropertyLength(todo) }}

    if currentLength > maxLength {
        return maxLength }
    return currentLength
}
func padOrEllipse(str string, length int) string {
    if len(str) == length {
        return str
    } else if len(str) < length {
        tmp := []rune(strings.Repeat(" ", length))
        if len(str) == 0 { return string(tmp) }
        if arguments.ListCenter {
            copy(tmp[(length-len(str))/2:], []rune(str))
        } else {
            copy(tmp[0:len(str)], []rune(str)) }
        return string(tmp)
    } else {
        tmp := []rune(strings.Repeat(".", length))
        copy(tmp[0:len(tmp)-3], []rune(str)[0:length-3])
        return string(tmp) }
}
