package main

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/atotto/clipboard"
	"gitlab.com/caulimaniac/td/Urgency"
)

var (
    linkRegex = regexp.MustCompile(`^https?://([\w_-]+\.)?[\w_-]+\.[\w_-]+(/[^ ]+)$`)
    titleRegex = regexp.MustCompile(`<title>(.+?)</title>`)
    userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.3"
)

func parseDeadline(todo *Todo) error {
    var err error

    currentDate   := getCurrentDate()
    assembledDate := currentDate
    dateRegex     := regexp.MustCompile(`^(\d{4})?-(\d{2})?-(\d{2})?$`)
    durationRegex := regexp.MustCompile(`^[pP]?((\d+)[yY])?((\d+)[mM])?((\d+)[wW])?((\d+)[dD])?$`)

    if dateRegex.MatchString(arguments.AddDeadline) {
        deadline := strings.Replace(
            arguments.AddDeadline, "^--", 
            fmt.Sprintf("%d-%d-", currentDate.Year(), currentDate.Month()), 1)
        deadline = strings.Replace(
            arguments.AddDeadline, "^-", 
            fmt.Sprintf("%d-", currentDate.Year()), 1)
        assembledDate, err = time.Parse(time.DateOnly, deadline)
        if err != nil { return err }

    } else if durationRegex.MatchString(arguments.AddDeadline) {
        matches := durationRegex.FindAllStringSubmatch(arguments.AddDeadline, 1)

        years,  _ := strconv.Atoi(matches[0][2])
        months, _ := strconv.Atoi(matches[0][4])
        weeks,  _ := strconv.Atoi(matches[0][6])
        days,   _ := strconv.Atoi(matches[0][8])
        days      += weeks * 7

        assembledDate = assembledDate.AddDate(years, months, days)

    } else {
        return errors.New("Unknown date format: " + arguments.AddDeadline) 
    }

    if assembledDate.Compare(currentDate) < 0 {
        return errors.New("Date shouldn't be in the past: " + assembledDate.Format(time.DateOnly)) }

    todo.Deadline = assembledDate.Format(time.DateOnly)
    return nil
}

func getWebsiteTitle(url string) (string, error) {
    client := &http.Client{}
    req, err := http.NewRequest("GET", url, nil)
    if err != nil { return "", err }

    req.Header.Set("User-Agent", userAgent)
    response, err := client.Do(req)
    if err != nil { return "", err }
    website, _ := io.ReadAll(response.Body)

    matches := titleRegex.FindAllStringSubmatch(string(website), 1)
    if len(matches) < 1 { return "", errors.New("No title element in html") }

    return matches[0][1], nil
}

func actionAdd(todoName string){
    var err error

    todo := &Todo{
        Name: todoName,
        Action: arguments.AddAction,
        Urgency: Urgency.LOW }

    if linkRegex.MatchString(todo.Name) {
        if newName, err := getWebsiteTitle(todo.Name); err == nil {
            if todo.Action == "" {
                todo.Action = "xdg-open " + todo.Name
                todo.Name = newName
            } else {
                todo.Name = newName + " (" + todo.Name + ")" }}}

    if arguments.AddDeadline != "" {
        err = parseDeadline(todo)
        checkError(err)
        if arguments.AddUrgency == Urgency.UNRATED {
            arguments.AddUrgency = Urgency.AUTO }}

    if arguments.AddUrgency != Urgency.UNRATED {
        todo.Urgency = arguments.AddUrgency }

    if len(todo.Name) == 0 {
        todo.Name, err = clipboard.ReadAll()
        checkError(err) }

    if len(arguments.AddTags) > 0 {
        todo.Tags = arguments.AddTags }

    todos = append(todos, todo)
}

func fallbackString(first string, fallbacks ...string) string {
    if first != "" { return first }
    for _, str := range fallbacks { if str != "" { return str } }
    return ""
}

func actionServeAdd(response http.ResponseWriter, request *http.Request){
    var err error
    queries := request.URL.Query()

    fetchTodos()

    todoName := fallbackString(queries.Get("name"), queries.Get("n"))
    todoTags := fallbackString(queries.Get("tags"), queries.Get("t"))
    todoDeadline := fallbackString(queries.Get("deadline"), queries.Get("d"))
    todoAction := fallbackString(queries.Get("action"), queries.Get("a"))
    todoUrgency := fallbackString(queries.Get("urgency"), queries.Get("u"))

    if todoName     == "" { response.WriteHeader(http.StatusBadRequest); return }
    if todoTags     != "" { arguments.AddTags     = strings.Split(todoTags, ",") }
    if todoDeadline != "" { arguments.AddDeadline = todoDeadline }
    if todoAction   != "" { arguments.AddAction   = todoAction }
    if todoUrgency  != "" { 
        arguments.AddUrgency, err = Urgency.ParseString(todoUrgency)
        if err != nil { 
            response.WriteHeader(http.StatusUnprocessableEntity); 
            return }}

    actionAdd(todoName)
    writeChanges()
}
