package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/caulimaniac/td/Term"
	"gitlab.com/caulimaniac/td/Urgency"
	"golang.org/x/exp/slices"
)

const (
    ActionAdd string = "a"
    ActionRemoveManual string = "rm"
    ActionRemoveExpired string = "re"
    ActionRemoveAll string = "ra"
)

type Action struct {
    Todo *Todo
    Time time.Time
    Action string
}

func displayHistorySimple(history []Action) {
    for _, action := range history {
        prefix := Term.COLOR_GREEN + "+"
        if action.Action != ActionAdd { prefix = Term.COLOR_RED + "-" }
        fmt.Printf("%s%s%s\n", prefix, action.Todo.Name, Term.CLEAR_FORMAT)
    }
}

func actionToString(action string) string {
    switch action {
    case ActionAdd:
        return "Added"
    case ActionRemoveAll:
        return "Removed all"
    case ActionRemoveExpired:
        return "Removed expired"
    case ActionRemoveManual:
        return "Removed" }
    return "Unknown"
}

func displayHistoryList(history []Action) {
    todoSuffix := "\n\n"
    lastTime := time.Time{}
    for actionIndex, action := range history {
        itemToPrint := ""
        if action.Time != lastTime {
            prefix := Term.COLOR_GREEN
            if action.Action != ActionAdd { prefix = Term.COLOR_RED }

            itemToPrint = fmt.Sprintf("%s%s%s on %s\n", 
                prefix, actionToString(action.Action), Term.CLEAR_FORMAT,
                action.Time.Format("2006-01-01 15:04:05")) }
        
        itemToPrint += fmt.Sprintf("\t%s%s%s\n\tUrgency: %s", 
            Term.STYLE_BOLD, action.Todo.Name, Term.CLEAR_FORMAT,
            Urgency.ToString(action.Todo.Urgency))
        if len(action.Todo.Tags) > 0 {
            itemToPrint = fmt.Sprintf("%s\n\tTags: %s",
                itemToPrint, strings.Join(action.Todo.Tags, ", ")) }
        if action.Todo.Deadline != "" {
            itemToPrint = fmt.Sprintf("%s\n\tDeadline: %s",
                itemToPrint, action.Todo.Deadline) }

        if actionIndex == len(history) - 1 { todoSuffix = "\n" }
        fmt.Printf("%s%s", itemToPrint, todoSuffix)

        lastTime = action.Time
    }
}

func displayHistory() {
    historyJson, err := os.ReadFile(arguments.Config + "/history.json")
    checkError(err)

    var history []Action
    json.Unmarshal(historyJson, &history)

    slices.SortFunc(history, func(a Action, b Action) int { 
        return a.Time.Compare(b.Time) })

    if arguments.HistoryList {
        displayHistoryList(history)
    } else {
        displayHistorySimple(history)
    }
}

func getActions() (actions []Action) {
    actionTime := time.Now()
    oldTodosJson, err := os.ReadFile(arguments.Config + "/todo.json")
    checkError(err)
    var oldTodos []*Todo
    json.Unmarshal(oldTodosJson, &oldTodos)

    for _, oldTodo := range oldTodos {
        stillPresent := false
        for _, todo := range todos {
            if todo.Name == oldTodo.Name {
                stillPresent = true }}
        if !stillPresent {
            action := ActionRemoveManual
            if arguments.DoneExpired { action = ActionRemoveExpired }
            if arguments.DoneAll     { action = ActionRemoveAll     }
            actions = append(actions, Action{
                Todo: oldTodo,
                Action: action,
                Time: actionTime })}}

    for _, todo := range todos {
        newlyPresent := true
        for _, oldTodo := range oldTodos {
            if todo.Name == oldTodo.Name {
                newlyPresent = false }}
        if newlyPresent {
            actions = append(actions, Action{
                Todo: todo,
                Action: ActionAdd,
                Time: actionTime })}}

    return
}

func rewriteHistory() {
    historyJson, err := os.ReadFile(arguments.Config + "/history.json")
    checkError(err)

    var history []Action
    json.Unmarshal(historyJson, &history)

    history = append(history, getActions()...)

    writeHistory(history)
}

func writeHistory(history []Action){
    historyJson, err := json.Marshal(history)
    checkError(err)

    os.WriteFile(arguments.Config + "/history.json", historyJson, 0755)
}

