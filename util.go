package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/caulimaniac/td/Urgency"
)

func checkError(err error) {
    if err == nil { return }

    if arguments.Debug { 
        panic(err) 
    } else {
        fmt.Println(err.Error()) }

    os.Exit(1)
}

func fetchTodos() {
    todoJson, err := os.ReadFile(arguments.Config + "/todo.json")
    checkError(err)
    json.Unmarshal(todoJson, &todos)
}

func setup() {
    if _, err := os.Stat(arguments.Config); err != nil {
        os.MkdirAll(filepath.Dir(arguments.Config), 0755) }
    if _, err := os.Stat(arguments.Config + "/history.json"); err != nil {
        os.WriteFile(arguments.Config + "/history.json", []byte("[]"), 0755)
    }
    if _, err := os.Stat(arguments.Config + "/todo.json"); err != nil {
        os.WriteFile(arguments.Config + "/todo.json", []byte("[]"), 0755)
    }
}

func writeChangesWithHistory() {
    rewriteHistory()

    writeChanges()
}

func writeChanges() {
    todoJson, _ := json.Marshal(todos)
    os.WriteFile(arguments.Config + "/todo.json", todoJson, 0644)
}

// Sorts todos like so:
//   urgency        (most urgent first)
//   deadline       (most recent first)
//   has deadline   (has deadline first)
//   alphabetically
func sortTodosIntuitively(i, j int) bool {
    if todos[i].Urgency != todos[j].Urgency {
        return Urgency.LowerThan(todos[i].Urgency, todos[j].Urgency) }
    
    if len(todos[i].Deadline) != 0 && len(todos[j].Deadline) != 0 {
        iDate, _ := time.Parse(time.DateOnly, todos[i].Deadline)
        jDate, _ := time.Parse(time.DateOnly, todos[j].Deadline)
        return iDate.Compare(jDate) < 1 }
    
    if len(todos[i].Deadline) != 0 {
        return true } // Ensure element with deadline are listed before 
                      // elements without

    return strings.Compare(
        strings.ToLower(todos[i].Name), 
        strings.ToLower(todos[j].Name)) < 0
}

func getCurrentDate() time.Time {
    return time.Now().Add(-12 * time.Hour).Round(24 * time.Hour)
}

func parseAutoUrgencies(){
    for _, todo := range todos {
        if todo.Urgency == Urgency.AUTO {
            deadline, _ := time.Parse(time.DateOnly, todo.Deadline)
            difference := deadline.Sub(getCurrentDate())
            if difference.Hours() > 24 * 30 {
                todo.Urgency = Urgency.LOW
            } else if difference.Hours() > 24 * 7 {
                todo.Urgency = Urgency.NORMAL
            } else {
                todo.Urgency = Urgency.CRITICAL }
        } else if todo.Urgency == Urgency.UNRATED {
            todo.Urgency = Urgency.LOW }}
}

func generateDeadlinePostfix(todo *Todo) string {
    if len(todo.Deadline) == 0 { return "" }

    deadline, _ := time.Parse(time.DateOnly, todo.Deadline)
    daysToDeadline := int(deadline.Sub(getCurrentDate()).Hours() / 24)
    if daysToDeadline < 0 {
        return " (expired)"
    } else if daysToDeadline == 0 {
        return " (due today)"
    } else if daysToDeadline == 1 {
        return " (due tomorrow)"
    } else if daysToDeadline <= 30 {
        return fmt.Sprintf(" (due in %d days)", daysToDeadline) 
    } else {
        return " (" + todo.Deadline + ")" }
}
