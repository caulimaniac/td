package main

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/ktr0731/go-fuzzyfinder"
	"gitlab.com/caulimaniac/td/Urgency"
	"golang.org/x/term"
)

func toAlphaNumeric(str string) string {
    alphaNumericRegex := regexp.MustCompile(`[^A-Za-z0-9 ]+`)
    return alphaNumericRegex.ReplaceAllString(str, "")
}

func isAlphaNumeric(str string) bool {
    alphaNumericRegex := regexp.MustCompile(`^[A-Za-z0-9 ]+$`)
    return alphaNumericRegex.MatchString(str)
}

func isLower(str string) bool {
    return str == strings.ToLower(str)
}

func matchFormat(template string, target string) string {
    newString := target
    if isAlphaNumeric(template) { newString = toAlphaNumeric(newString)  }
    if isLower(template)        { newString = strings.ToLower(newString) }
    return newString
}

func promptForTodos() ([]int, error) {
    return fuzzyfinder.FindMulti(todos,
        func(i int) string { return todos[i].Name },
        fuzzyfinder.WithPreviewWindow(func(i, width, height int) string {
            if i == -1 { return "" }
            return fmt.Sprintf(
                "Name: %s\nUrgency: %s\nTags: %s\nAction: %s\nDeadline: %s",
                todos[i].Name, 
                Urgency.ToString(todos[i].Urgency),
                strings.Join(todos[i].Tags, ", "), 
                strings.SplitN(todos[i].Action, " ", 2)[0],
                todos[i].Deadline )}))
}

func splitTodos(decision func(*Todo)bool) ([]*Todo, []*Todo) {
    primaryTodos := []*Todo{}
    secondaryTodos := []*Todo{}

    for _, todo := range todos {
        if decision(todo) {
            primaryTodos = append(primaryTodos, todo)
        } else {
            secondaryTodos = append(secondaryTodos, todo) }}

    return primaryTodos, secondaryTodos
}

func actionDone(){
    todosToRemove := []*Todo{}
    warnMultipleRemove := false

    if arguments.DoneAll { // Remove all todos
        if arguments.DoneYes {
            todosToRemove = todos
            todos = []*Todo{}
        } else {
            fmt.Printf("You are about to delete all userdata. Continue? [y/N] ")
            var confirm string
            fmt.Scanln(&confirm)
            if strings.EqualFold(confirm, "y") {
                todosToRemove = todos }}

    } else if arguments.DoneExpired { // Remove expired todos
        todosToRemove, todos = splitTodos(func(todo *Todo) bool {
            if todo.Deadline == "" { return false }
            deadlineTime, err := time.Parse(time.DateOnly, todo.Deadline)
            checkError(err)
            return getCurrentDate().Compare(deadlineTime) > 0 })
        if len(todosToRemove) < 1 {
            fmt.Println("No expired todos to remove") }

    } else if len(arguments.Trailing) > 2 {
        for _, query := range arguments.Trailing[2:] {
            var plainMatch, letterMatch []*Todo
            plainMatch, todos = splitTodos(func(todo *Todo) bool {
                return strings.Contains(strings.ReplaceAll(
                    matchFormat(query, todo.Name), " ", ""), query) })
    
            letterMatch, todos = splitTodos(func(todo *Todo) bool {
                todoName := matchFormat(query, todo.Name)
                firstChars := string(todoName[0])
                for i := 1; i < len(todoName) - 1; i++ {
                    if todoName[i] == ' ' && todoName[i + 1] != ' ' {
                        firstChars += string(todoName[i + 1]) }}
                return strings.Contains(firstChars, query) })

            if len(plainMatch) + len(letterMatch) == 0 {
                fmt.Printf("No todo matched expression '%s'\n", query)
                continue }

            if len(plainMatch) > 0 {
                todosToRemove = append(todosToRemove, plainMatch...)
                if len(plainMatch) > 1 { warnMultipleRemove = true }
                todos = append(todos, letterMatch...)
            } else {
                todosToRemove = append(todosToRemove, letterMatch...) }
                if len(letterMatch) > 1 { warnMultipleRemove = true }}
    } else {
        indexesToRemove, err := promptForTodos()
        checkError(err)

        todosToKeep := []*Todo{}
        for todoIndex, todo := range todos {
            keepTodo := true
            for _, indexesToRemove := range indexesToRemove {
                if todoIndex == indexesToRemove {
                    keepTodo = false }}
            if keepTodo {
                todosToKeep = append(todosToKeep, todo)
            } else {
                todosToRemove = append(todosToRemove, todo) }}

        todos = todosToKeep
    }

    if warnMultipleRemove {
        if !confirmRemove(todosToRemove) {
            todos = append(todos, todosToRemove...)
            todosToRemove = []*Todo{} }}

    if arguments.DoneNoAction { return }
    triggerActions(todosToRemove)
}

func confirmRemove(todosToRemove []*Todo) bool {
    fmt.Print("One expression provided matched multiple todos.\n")
    fmt.Print("The following todos have been selected for deletion:\n\n")
    termWidth, _, _ := term.GetSize(0)
    for todoIndex, todo := range todosToRemove {
        temporaryName := fmt.Sprintf("  %2d: %s", todoIndex, todo.Name) 
        for len(temporaryName) > termWidth {
            fmt.Println(temporaryName[0:termWidth])
            temporaryName = "      " + temporaryName[termWidth:] }
        fmt.Println(temporaryName)
        
    }
    fmt.Print("\nDo you want to delete all these todos? [Y/n] ")

    var confirm string
    fmt.Scanln(&confirm)
    if confirm == "" || strings.EqualFold(confirm, "y") { return true }

    return false
}

