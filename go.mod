module gitlab.com/caulimaniac/td

go 1.21.3

require (
	github.com/adrg/xdg v0.4.0
	github.com/atotto/clipboard v0.1.4
	github.com/gerow/pager v0.0.0-20190420205801-6d4a2327822f
	github.com/ktr0731/go-fuzzyfinder v0.8.0
	golang.org/x/exp v0.0.0-20231110203233-9a3e6036ecaa
	golang.org/x/term v0.14.0
	golang.org/x/tools v0.15.0
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell/v2 v2.6.0 // indirect
	github.com/ktr0731/go-ansisgr v0.1.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	github.com/stretchr/testify v1.8.3 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
