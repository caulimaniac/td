package Urgency

import (
	"errors"
	"regexp"
	"unicode"
)

type Urgency string
const (
    CRITICAL Urgency = "critical"
    NORMAL Urgency = "normal"
    LOW Urgency = "low"
    UNRATED = "unrated"
    AUTO = "auto"
)

func LowerThan(firstUrgency Urgency, secondUrgency Urgency) bool {
    urgencyToInt := map[Urgency]int {
        LOW: 1,
        NORMAL: 2,
        CRITICAL: 3,
        AUTO: 0,
        UNRATED: 0 }
    return urgencyToInt[firstUrgency] > urgencyToInt[secondUrgency]
}

func ParseString(str string) (Urgency, error) {
    regex := regexp.MustCompile(`^(low|l|normal|n|critical|c)$`)
    isValidUrgency := regex.MatchString(str)

    if !isValidUrgency {
        return UNRATED, errors.New("Invalid urgency, valid urgencies are low, normal, critical") }

    stringToUrgency := map[string]Urgency {
        "low": LOW,
        "l": LOW,
        "normal": NORMAL,
        "n": NORMAL,
        "critical": CRITICAL,
        "c": CRITICAL }
    
    return stringToUrgency[str], nil
}

func ToString(urgency Urgency) string {
    tmp := []rune(urgency)
    tmp[0] = unicode.ToUpper(tmp[0])
    return string(tmp)
}
