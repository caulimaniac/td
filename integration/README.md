# Integration

## Setup

td ships with a lightweight http server, whose sole purpose is allowing http
get request to add todos. The server is intended for applications which do not
have access to run commands.

In order to start the server manually, run `td serve`.
It is recommended to use systemd to run it automatically
`systemctl --user enable --now td.service`.

## Applications

### MPV

When watching a video with mpv, you can save the current state in td and resume it by finishing the todo.
You can put the following line into your `$XDG_CONFIG_HOME/mpv/input.conf`:
```
t run "/usr/bin/bash" "-c" 'td add "${media-title} (${path})" --urgency=low --tags=mpv --action="mpv \"${path}\" --start=${=time-pos}"'
```

### Zathura

In order to save a pdf with the current page to td, place this line into 
`$XDG_CONFIG_HOME/zathura/zathurarc`:
```
map t exec 'td add "$FILE" --action="zathura \"$FILE\" --page=$PAGE"'
```

### Browser

An example userscript can be found [here](userscript.js). On keypress it will
save the current tab to td.
This script requires the web server to be running.

## Adding New Applications

If you want to write a new implementation for another program, the programm 
has to be able to either
- execute commands
- or send http get requests.

The todo's action can be used in order to restart the program, 
preferably with the same state (time, page, etc.).

You may use this api endpoint to communicate with the api and add a todo:
`http://<host>:<port>/add?name=<name>[&tags=<tags>][&urgency=<urgency>][&action=<action>]`
The default host and port are `localhost:7464`.
