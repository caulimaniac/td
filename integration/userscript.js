// ==UserScript==
// @name     TD Integration
// @version  1.0
// @grant    none
// ==/UserScript==

const KEY = 'e'

const TODO_NAME = '%title%'
const TODO_TAGS = 'web'
const TODO_URGENCY = 'low'
const TODO_ACTION = 'xdg-open "%location%"'

const HOST = 'localhost'
const PORT = '7464'

const ERROR_MESSAGE = `Failed to add ToDo (status %status%)!

Could it be that...
    1. the server isn't running or
    2. the host and port aren't configured properly?

If you believe this to be a problem with td or this script,
please open an issue at https://gitlab.com/caulimaniac/td.`;

function insertPlaceholders(str){
    return str
        .replaceAll("%location%", document.location)
        .replaceAll("%title%",    document.title)
}

document.addEventListener('keydown', function(event) {
    if (event.ctrlKey && event.key === KEY) {
        var url = new URL(`http://${HOST}:${PORT}/add`);
        const params = new URLSearchParams({
            name: insertPlaceholders(TODO_NAME) || insertPlaceholders("%location%"),
            tags: insertPlaceholders(TODO_TAGS),
            urgency: TODO_URGENCY,
            action: insertPlaceholders(TODO_ACTION)
        });
        var request = new XMLHttpRequest()
        request.open("GET", url + "?" + params.toString())
        request.onreadystatechange = function receiveResponse() {
            if (this.readyState == 4) {
                if (this.status != 200) {
                    alert(ERROR_MESSAGE.replaceAll("%status%", this.status))}}}
        request.send();
    }
});
