package main

import (
	"errors"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/adrg/xdg"
	"gitlab.com/caulimaniac/td/Urgency"
	"golang.org/x/exp/slices"
)

type ArgumentList map[*[]string]func(string)

var (
    unprocessedArguments = os.Args

    arguments = struct {
        Trailing     []string
        Help         bool
        Debug        bool
        Config       string
        AddUrgency   Urgency.Urgency
        AddAction    string
        AddTags      []string
        AddDeadline  string
        ListUrgency  Urgency.Urgency
        ListNoColor  bool
        ListNoPager  bool
        ListJson     bool
        ListCompact  bool
        ListCenter   bool
        ListAll      bool
        DoneNoAction bool
        DoneAll      bool
        DoneAllYes   bool
        DoneExpired  bool
        ServePort    string
        ServeHost    string
    }{
        Trailing:     []string{},
        Help:         false,
        Debug:        false,
        Config:       xdg.DataHome + "/td/todo.json",
        AddUrgency:   Urgency.UNRATED,
        AddAction:    "",
        AddTags:      []string{},
        AddDeadline:  "",
        ListUrgency:  Urgency.UNRATED,
        ListJson:     false,
        ListCompact:  false,
        ListCenter:   false,
        ListNoColor:  false,
        ListNoPager:  false,
        ListAll:      false,
        ServePort:    "7464",
        ServeHost:    "localhost",
        DoneNoAction: false,
        DoneAll:      false,
        DoneAllYes:   false,
        DoneExpired:  false }

    ARGUMENTS_GLOBAL = ArgumentList {
        {"-h", "--help"  }: func(s string) { arguments.Help   = true },
        {"--debug"       }: func(s string) { arguments.Debug  = true },
        {"-c", "--config"}: func(s string) { 
            var err error
            expandedHome := s
            if strings.HasPrefix(s, "~") {
                expandedHome = strings.Replace(s, "~", xdg.Home, 1) }
            arguments.Config, err = filepath.Abs(expandedHome) 
            checkError(err) }}

    ARGUMENTS_LIST = ArgumentList {
        {"-o", "--compact"  }: func(s string) { arguments.ListCompact = true },
        {"-j", "--json"     }: func(s string) { arguments.ListJson    = true },
        {"-l", "--no-color" }: func(s string) { arguments.ListNoColor = true },
        {"-p", "--no-pager" }: func(s string) { arguments.ListNoPager = true },
        {"-e", "--center"   }: func(s string) { arguments.ListCenter  = true },
        {"-a", "--all"      }: func(s string) { arguments.ListAll     = true },
        {"-u", "--urgency"  }: func(s string) { 
            var err error
            arguments.ListUrgency, err = Urgency.ParseString(s)
            checkError(err) }}

    ARGUMENTS_ADD = ArgumentList {
        {"-t", "--tags"     }: func(s string) { arguments.AddTags     = strings.Split(s, ",") },
        {"-a", "--action"   }: func(s string) { arguments.AddAction   = s },
        {"-d", "--deadline" }: func(s string) { arguments.AddDeadline = s },
        {"-u", "--urgency"  }: func(s string) { 
            var err error
            arguments.AddUrgency, err = Urgency.ParseString(s)
            checkError(err) }}

    ARGUMENTS_DONE = ArgumentList {
        {"-e", "--expired"   }: func(s string) { arguments.DoneExpired  = true },
        {"-n", "--no-action" }: func(s string) { arguments.DoneNoAction = true },
        {"-a", "--all"       }: func(s string) { arguments.DoneAll      = true },
        {"-y", "--yes"       }: func(s string) { arguments.DoneAllYes   = true }}

    ARGUMENTS_SERVE = ArgumentList {
        {"-n", "--host"}: func(s string) { arguments.ServeHost = s },
        {"-p", "--port"}: func(s string) { arguments.ServePort = s }}

    ARGUMENTS_HELP = ArgumentList {}
    ARGUMENTS_UNDO = ArgumentList {}
)

// td -am => td -a -m
func expandSquashedArguments(){
    squashedArguments := regexp.MustCompile(`^-([A-z]){2}`)
	expandedArguments := []string{}

	for _, argument := range unprocessedArguments {
		for squashedArguments.FindStringSubmatch(argument) != nil {
			expandedArguments = append(expandedArguments, argument[0:2])
			argument = "-" + argument[2:]
		}
		expandedArguments = append(expandedArguments, argument)
	}

    unprocessedArguments = expandedArguments
}

func parseArguments(argumentList ArgumentList, allowUnknown bool){
    unknownArguments := []string{}

    for _, argument := range unprocessedArguments {
        if strings.HasPrefix(argument, "-") {

            var key, value string
            if len(strings.SplitN(argument, "=", 2)) == 2 {
                key = strings.Split(argument, "=")[0]
                value = strings.SplitN(argument, "=", 2)[1]
            } else {
                key = argument }

            argumentFound := false
            for argumentKeys, argumentFunction := range argumentList {
                if slices.Contains(*argumentKeys, key) {
                    argumentFunction(value)
                    argumentFound = true
                    break }}

            if argumentFound { continue }

            if allowUnknown { 
                unknownArguments = append(unknownArguments, argument) 
                continue }

            checkError(errors.New("Invalid argument: " + key)) 

        } else {
            arguments.Trailing = append(arguments.Trailing, argument) }}

    unprocessedArguments = unknownArguments
}

