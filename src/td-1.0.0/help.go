package main

import "fmt"

func displayHelp(){
    fmt.Println(
`arguments:
    -h, --help  : display help
    -c, --config: /path/to/todos.json
    --debug     : print traceback on error

commands:
    l, list [tag]
        -o, --compact : only show todo names
        -j, --json    : output json
        -l, --no-color: print todos without color
        -p, --no-pager: disables automatic paging
        -e, --center  : when using table, center all columns
        -a, --all     : shows todos prefixed with a period
        -u, --urgency : low|normal|critical
    a, add todo 
        -u, --urgency : low|normal|critical
        -t, --tags    : list of strings (comma separated)
        -a, --action  : string containing bash command to run on task completion
        -d, --deadline: YYYY-MM-DD|nYnMnWnD
    d, done [todo]
        -e, --expired  : clear expired todos
        -n, --no-action: don't trigger the action of completed todo
        -a, --all      : completes all todos
        -y, --yes      : don't ask for confirmation when completing all
    s, serve
        -n, --host: where to host at (default: localhost)
        -p, --port: which host to use (default: 7464)
    u, undo
    h, help

examples:
    td add buy printer ink --urgency=normal --deadline=2090-10-20
    td add assist in kernel development --urgency=critical --tags=development,linux
    td add setup wifi router --deadline=3D
    td list development
    td list art --urgency=critical --compact
    td clear`)
}
