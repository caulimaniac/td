package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/ktr0731/go-fuzzyfinder"
	"gitlab.com/caulimaniac/td/Urgency"
)

var (
    MULTI_ACTION_CONFIG = map[string]string{
        "mpv": " --{ %--start=[0-9]+% %--end=[0-9]+% %( |^)[^-][^ ]*% --}",
        "imv": " %( |^)[^-][^ ]*%",
        "test": " %( |^)[^-][^ ]*%" }
)

func promptForTodos() ([]int, error){
    return fuzzyfinder.FindMulti(todos,
        func(i int) string { return todos[i].Name },
        fuzzyfinder.WithPreviewWindow(func(i, width, height int) string {
            if i == -1 { return "" }
            return fmt.Sprintf("Name: %s\nUrgency: %s\nTags: %s\nAction: %s\nDeadline: %s",
                todos[i].Name, Urgency.ToString(todos[i].Urgency), 
                strings.Join(todos[i].Tags, ", "), todos[i].Action, 
                todos[i].Deadline )}))
}

func actionUndo(){
    backup, err := os.ReadFile(arguments.Config + ".bak")
    if err != nil { checkError(errors.New("No backup to revert to")) }

    os.WriteFile(arguments.Config, backup, 0644)
    os.Remove(arguments.Config + ".bak")
    fmt.Println("Restored backup")
}

func actionServe(){
    ginRouter := gin.Default()
    ginRouter.GET("/add", actionServeAdd)
    ginRouter.Run(arguments.ServeHost + ":" + arguments.ServePort)
}

func actionDone(){
    var temporaryTodos []*Todo
    var todosToRemove []*Todo

    if arguments.DoneAll { // Remove all todos
        if arguments.DoneAllYes {
            todosToRemove = todos 
        } else {
            fmt.Printf("You are about to delete all userdata. Continue? [y/N] ")
            var confirm string
            fmt.Scanln(&confirm)
            if !strings.EqualFold(confirm, "y") {
                temporaryTodos = todos 
            } else {
                todosToRemove = todos }}

    } else if len(arguments.Trailing) > 2 { // Remove todos with name
        for _, todo := range todos {
            if todo.Name != arguments.Trailing[2] {
                temporaryTodos = append(temporaryTodos, todo) 
            } else { todosToRemove = append(todosToRemove, todo) }}

    } else if arguments.DoneExpired { // Remove expired todos
        for _, todo := range todos {
            if todo.Deadline == "" { 
                temporaryTodos = append(temporaryTodos, todo)
                continue }

            deadlineTime, err := time.Parse(time.DateOnly, todo.Deadline)
            checkError(err)
            if getCurrentDate().Compare(deadlineTime) <= 0 {
                temporaryTodos = append(temporaryTodos, todo) }}

    } else { // Prompt for todos to remove
        indexesToRemove, err := promptForTodos()
        if err != nil { fmt.Println(err.Error()) }

        for todoIndex, todo := range todos {
            remove := false
            for _, indexToRemove := range indexesToRemove {
                if todoIndex == indexToRemove {
                    remove = true
                    todosToRemove = append(todosToRemove, todos[indexToRemove]) }}
    
            if !remove {
                temporaryTodos = append(temporaryTodos, todo) }}}

    todos = temporaryTodos
    if arguments.DoneNoAction { return }
    triggerActions(todosToRemove)
}

func triggerActions(todos []*Todo) {
    programCounts := map[string]int{}
    for _, todo := range todos {
        programName := strings.SplitN(todo.Action, " ", 2)[0]
        programCounts[programName]++ }

    multiCommands := map[string]string{}
    for _, todo := range todos {
        if todo.Action != "" {
            programName := strings.SplitN(todo.Action, " ", 2)[0]
            if programCounts[programName] < 2 || len(MULTI_ACTION_CONFIG[programName]) == 0 {
                err := exec.Command("/usr/bin/bash", "-c", todo.Action).Start()
                checkError(err) 
                continue }

            if len(multiCommands[programName]) == 0 { 
                multiCommands[programName] = programName }

            multiCommands[programName] += assembleMultiParameter(
                MULTI_ACTION_CONFIG[programName],
                strings.SplitN(todo.Action, " ", 2)[1]) }}

    for _, multiCommand := range multiCommands {
        err := exec.Command("/usr/bin/bash", "-c", multiCommand).Start()
        checkError(err) }
}

func assembleMultiParameter(argumentBlueprint string, arguments string) string {
    parsedConfig := argumentBlueprint

    regexSection := regexp.MustCompile(`%([^%]*)%`)
    matches := regexSection.FindAllStringSubmatch(parsedConfig, -1)

    for _, regexScript := range matches {
        customRegex := regexp.MustCompile(regexScript[1])
        parsedConfig = strings.ReplaceAll(
            parsedConfig,
            regexScript[0],
            customRegex.FindString(arguments)) }

    return parsedConfig
}
