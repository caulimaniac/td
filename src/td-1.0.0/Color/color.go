package Color

import (
    "gitlab.com/caulimaniac/td/Urgency"
)

type Color string
const (
    RED Color = "\033[31m"
    YELLOW Color = "\033[33m"
    GREEN Color = "\033[32m"
    CLEAR Color = "\033[0m"
    BOLD Color = "\033[1m"
)

func UrgencyToColor(urgencyToMap Urgency.Urgency) Color {
    urgencyColorMap := map[Urgency.Urgency]Color {
        Urgency.CRITICAL: RED,
        Urgency.NORMAL: YELLOW,
        Urgency.LOW: GREEN }
    return urgencyColorMap[urgencyToMap]
}
