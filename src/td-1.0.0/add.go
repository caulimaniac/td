package main

import (
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/atotto/clipboard"
	"github.com/gin-gonic/gin"
	"gitlab.com/caulimaniac/td/Urgency"
)

func applyAutoTags(todo *Todo){
    containsLink := regexp.MustCompile(`https?://`)

    if arguments.AddAction != "" {
        todo.Tags = append(todo.Tags, "action") }
    if containsLink.Match([]byte(todo.Name)) {
        todo.Tags = append(todo.Tags, "link") }
}

func parseDeadline(todo *Todo) error {
    var err error

    currentDate   := getCurrentDate()
    assembledDate := currentDate
    dateRegex     := regexp.MustCompile(`^(\d{4})?-(\d{2})?-(\d{2})?$`)
    durationRegex := regexp.MustCompile(`^[pP]?((\d+)[yY])?((\d+)[mM])?((\d+)[wW])?((\d+)[dD])?$`)

    if dateRegex.MatchString(arguments.AddDeadline) {
        deadline := strings.Replace(
            arguments.AddDeadline, "^--", 
            fmt.Sprintf("%d-%d-", currentDate.Year(), currentDate.Month()), 1)
        deadline = strings.Replace(
            arguments.AddDeadline, "^-", 
            fmt.Sprintf("%d-", currentDate.Year()), 1)
        assembledDate, err = time.Parse(time.DateOnly, deadline)
        if err != nil { return err }

    } else if durationRegex.MatchString(arguments.AddDeadline) {
        matches := durationRegex.FindAllStringSubmatch(arguments.AddDeadline, 1)

        years,  _ := strconv.Atoi(matches[0][2])
        months, _ := strconv.Atoi(matches[0][4])
        weeks,  _ := strconv.Atoi(matches[0][6])
        days,   _ := strconv.Atoi(matches[0][8])
        days      += weeks * 7

        assembledDate = assembledDate.AddDate(years, months, days)

    } else {
        return errors.New("Unknown date format: " + arguments.AddDeadline) 
    }

    if assembledDate.Compare(currentDate) < 0 {
        return errors.New("Date shouldn't be in the past: " + assembledDate.Format(time.DateOnly)) }

    todo.Deadline = assembledDate.Format(time.DateOnly)
    return nil
}

func actionAdd(todoName string){
    var err error

    todo := &Todo{
        Name: todoName,
        Action: arguments.AddAction,
        Urgency: Urgency.LOW }

    if arguments.AddDeadline != "" {
        err = parseDeadline(todo)
        checkError(err)
        if arguments.AddUrgency == Urgency.UNRATED {
            arguments.AddUrgency = Urgency.AUTO }}

    if arguments.AddUrgency != Urgency.UNRATED {
        todo.Urgency = arguments.AddUrgency }

    if len(todo.Name) == 0 {
        todo.Name, err = clipboard.ReadAll()
        checkError(err) }

    if len(arguments.AddTags) > 0 {
        todo.Tags = arguments.AddTags }
    applyAutoTags(todo)

    todos = append(todos, todo)
}

func actionServeAdd(context *gin.Context){
    var err error
    fetchTodos()

    todoName := context.Query("name")
    if todoName == "" { context.Status(http.StatusBadRequest); return }

    todoUrgency := context.Query("urgency")
    if todoUrgency != "" { 
        arguments.AddUrgency, err = Urgency.ParseString(todoUrgency)
        if err != nil { context.Status(http.StatusUnprocessableEntity); return }}

    todoTags := context.Query("tags")
    if todoTags != "" {
        arguments.AddTags = strings.Split(todoTags, ",") }

    todoAction := context.Query("action")
    if todoAction != "" {
        arguments.AddAction = todoAction }

    todoDeadline := context.Query("deadline")
    if todoDeadline != "" {
        arguments.AddDeadline = todoDeadline }

    actionAdd(todoName)
    writeChanges()
}
