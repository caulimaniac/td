INSTALL_BINARY = ${PREFIX}/usr/bin/td
INSTALL_SERVICE = ${PREFIX}/etc/systemd/user/td.service
INSTALL_MANUAL = ${PREFIX}/usr/share/man/man1/td.1.gz
INSTALL_SVG = ${PREFIX}/usr/share/icons/hicolor/scalable/apps/td.svg
INSTALL_PNG = ${PREFIX}/usr/share/icons/hicolor/48x48/apps/td.svg
INSTALL_DESKTOP = ${PREFIX}/usr/share/applications/td.desktop

COMPILE_DIR = bin
COMPILE_BINARY = ${COMPILE_DIR}/td
COMPILE_MANUAL = ${COMPILE_DIR}/td.1
COMPILE_MANUAL_COMPRESSED = ${COMPILE_DIR}/td.1.gz

SOURCE_MANUAL = assets/td.1.scd
SOURCE_SERVICE = assets/td.service
SOURCE_SVG = assets/logo.svg
SOURCE_PNG = assets/logo.png
SOURCE_DESKTOP = assets/td.desktop

TEST_DIR = tmp
TEST_CONFIG = ${TEST_DIR}/td.json
TEST_FILE = ${TEST_DIR}/td.test
LDFLAGS = -s -w

build:
	go build -o ${COMPILE_BINARY} -ldflags="${LDFLAGS}"
	scdoc < ${SOURCE_MANUAL} > ${COMPILE_MANUAL}
	gzip -f ${COMPILE_MANUAL}

install:
	install -Dm644 ${SOURCE_SERVICE} ${INSTALL_SERVICE}
	install -Dm755 ${COMPILE_BINARY} ${INSTALL_BINARY}
	install -Dm644 ${COMPILE_MANUAL_COMPRESSED} ${INSTALL_MANUAL}
	install -Dm644 ${SOURCE_PNG} ${INSTALL_PNG}
	install -Dm644 ${SOURCE_SVG} ${INSTALL_SVG}

uninstall:
	rm ${INSTALL_SERVICE}
	rm ${INSTALL_BINARY}
	rm ${INSTALL_MANUAL}
	rm ${INSTALL_PNG}
	rm ${INSTALL_SVG}

test: build
	rm -rf ${TEST_DIR}
	mkdir ${TEST_DIR}
	${COMPILE_BINARY} -c=${TEST_CONFIG} --debug a test1 -u=l -d=2999-01-01 -a="test hi"
	${COMPILE_BINARY} -c=${TEST_CONFIG} --debug a test1 -u=l -a="test ho"
	${COMPILE_BINARY} -c=${TEST_CONFIG} --debug a test2 -u=n -t=sample
	${COMPILE_BINARY} -c=${TEST_CONFIG} --debug a test3 -u=c -a="echo test > ${TEST_FILE}"
	${COMPILE_BINARY} -c=${TEST_CONFIG} --debug l s -olpeau=l
	${COMPILE_BINARY} -c=${TEST_CONFIG} --debug d test
	${COMPILE_BINARY} -c=${TEST_CONFIG} --debug d test2 -n
	${COMPILE_BINARY} -c=${TEST_CONFIG} --debug d -ay
	${COMPILE_BINARY} -c=${TEST_CONFIG} --debug u
	cat ${TEST_FILE} | grep test >/dev/null

clean:
	rm -rf ${TEST_DIR}
	rm -rf ${BUILD_DIR}
